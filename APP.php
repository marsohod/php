<?php
	require_once("./libs/LOG.php");
	require_once("./libs/TRADER.php");
	require_once("./api/API.php");
	require_once("./api/APISTOCK2.php");
	require_once("./model/modelDiff.php");

/*
	TODO: check pump
*/

class APP {
	protected $pair;
	protected $pair2 			= 'btc';
	protected $onlyWatch		= false;
	protected $tradeAmount 		= 100;
	protected $percent 			= 0.5;
	protected $pause 			= 2;
	protected $notRealizedDiff 	= [];
	public $stocks 				= [];

	function __construct( $data, $onlyWatch = false ) {
		$this->pair 				= $data['pair'];
		$this->floatNumbers 		= isset($data['floatNumbers']) 		? $data['floatNumbers'] : 0;
		$this->pair2 				= isset($data['pair2']) 			? $data['pair2'] : $this->pair2;
		$this->percent 				= isset($data['percent']) 			? $data['percent'] : $this->percent;
		$this->tradeAmount 			= isset($data['tradeMaxAmount']) 	? $data['tradeMaxAmount'] : $this->tradeAmount;
		$this->minAmountDoWithdraw 	= isset($data['minAmountDoWithdraw']) 	? $data['minAmountDoWithdraw'] : 1;
		$this->defaultTradeAmount 	= $this->tradeAmount;
		$this->onlyWatch 			= $onlyWatch;
		$this->forwardMultiply		= isset($data['forwardMultiply'])	? $data['forwardMultiply'] : 3;

		$options = [
			"maxAmountForOrder" => $this->tradeAmount,
			"pair" 				=> $this->pair,
			"pair2" 			=> $this->pair2
		];

		if ( isset($data["STOCK1"]) ) {
			$this->stocks[]	= new API( array_merge( $data["STOCK1"], $options ));
		}

		if ( isset($data["STOCK2"]) ) {
			$this->stocks[]	= new APISTOCK2( array_merge( $data["STOCK2"], $options ));
		}

		$this->trader = new TRADER($data);

		print "START " . strtoupper($this->pair) . " expected diff " . $this->percent . "\n";
	}

	public function run ( $sleep = null, $params = null ) {
		if ( $sleep != null ) { sleep($sleep); }

		$this->trader->clearHistory();
		$diffs = $this->getDiffs();

		$this->logDiffs($diffs);
		
		if ( $this->onlyWatch ) {
			$this->run(1);
			return false;
		}

		if ( $diffs[0]->getPercent() >= $this->getSmartPercent($diffs[0]) ) {
			$this->trade($diffs[0]);
			return;
		}

		$this->clearDiffHistory();
		$this->run(1);
	}

	protected function getDiffs () {
		$out = [];
		$tickers = $this->getTickers();
		
		for( $i = 0; $i < count($tickers); $i++ ) {
			for( $j = $i+1; $j < count($tickers); $j++ ) {
				$out[] 	= $this->getDiff($tickers[$i], $tickers[$j], $this->stocks[$i]->getFee(), $this->stocks[$j]->getFee());
			}
		}

		return $out;
	}

	protected function getTickers () {
		return array_map( function ( $s ) { return $s->getTicker(); }, $this->stocks);
	}

	protected function getDiff ( $ticker1, $ticker2, $fee1 = 0, $fee2 = 0 ) {
		if ( !$ticker1 || !$ticker2 ) {
			LOG::getInstance()->sendWarning("Cant get TICKER from " . (!$ticker1 ? $this->buyer->printName() : '' ) . (!$ticker2 ? $this->seller->printName() : '' ) );
			return new Diff();
		}

		return $this->calculateDiff( $ticker1, $ticker2, $fee1, $fee2 );
	}

	protected function logDiffs ( $diffs ) {
		$badDiff 	= [];
		
		for( $i = 0; $i < count($diffs); $i++ ) {
			if ( $diffs[$i]->getPercent() > 0 ) {
				LOG::getInstance()->printDiff( $diffs[$i], $this->percent, $this->tradeAmount );
				// Log to DB
				// LOG::getInstance()->logDiff( $diffs[$i], $this->seller->getCoinNameByPair(), $this->tradeAmount );
				continue;
			}
			
			$badDiff[] = ".";	
		}

		if ( count($badDiff) > 0 ) { LOG::getInstance()->spinner(); }
	}

	protected function getIndexBestDiff ( $diffs ) {
		$index = 0;

		if ( isset($diffs[$i+1]) ) {
			if ( $diffs[$i+1]->getPercent() > $diffs[$index]->getPercent() ) {
				$index = $i+1;
			}
		}

		return index;
	}

	public function calculateDiff( $mI, $mB, $fee1 = 0, $fee2 = 0 ) {
		$d 	= 0;
		$p1 = $mB->getValueSell() == 0 ? 0 : $mI->getValueBuy() / $mB->getValueSell() - 1;
		$p2 = $mI->getValueSell() == 0 ? 0 : $mB->getValueBuy() / $mI->getValueSell() - 1;
		$p 	= max($p1, $p2)*100;

		return new Diff([
			"percent"	=> number_format((float)$p - $fee1 - $fee2, 2, '.', ''),
			"direction" => $mI->getStock() . ($p1 < $p2 ? " -> " : " <- ") . $mB->getStock(),
			"directionShort" => ($p1 < $p2 ? " -> " : " <- "),
			"priceSell" => $p1 < $p2 ? $mB->getValueBuy() : $mI->getValueBuy(),
			"priceBuy" 	=> $p1 < $p2 ? $mI->getValueSell() : $mB->getValueSell(),
			"amountSell"=> $this->trader->getAmount($p1 < $p2 ? $mB->getAmountBuy() : $mI->getAmountBuy()),
			"amountBuy"	=> $this->trader->getAmount($p1 < $p2 ? $mI->getAmountSell() : $mB->getAmountSell())			
		]);
	}

	public function getSmartPercent ( $diff ) {
		return $this->percent;
	}

	protected function setSmartTradeAmount ( $percent ) {
		$k = 1;

		$d = $percent - $this->percent;

		if ( $d > 0.3 ) { $k = 1.3; }
		if ( $d > 0.2 ) { $k = 1.1; }
		if ( $d >= 0 && $d <= 0.1 ) { $k = 0.8; }
		if ( $d < 0 ) { $k = 0.3; }

		$this->tradeAmount = $this->defaultTradeAmount*$k;
	}

	/**
	 * $diff - is modelDiff object
	 * return number or false
	 */
	private function getAmountForTrade( $diff ) {
		$this->seller 	= strpos($diff->getDirection(), "->") ? $this->stocks[1] : $this->stocks[0];
		$this->buyer 	= strpos($diff->getDirection(), "->") ? $this->stocks[0] : $this->stocks[1];

		//Check balance in COINs
		$coins 	= $this->trader->getAmount($this->seller->getBalance( $this->seller->getCoinNameByPair() ));
		
		//Check balance in BTC
		$btc 	= $this->buyer->getBalance( $this->pair2 );

		//Determine amount coins for trade
		$amount = $diff->getMinAmount($this->tradeAmount);

		if ( $coins < $amount ) {
			$this->notEnoughCoins($this->seller->getCoinNameByPair(), $diff, $this->buyer->getId() );
			LOG::getInstance()->sendWarning("Not enough " . strtoupper($this->seller->getCoinNameByPair()) . ": ". $coins ." in " . $this->seller->printName() . ". Tried trade amount: " . $amount);
			LOG::getInstance()->logDiff( $diff, $this->seller->getCoinNameByPair(), $this->tradeAmount );
			$amount = $coins;
		}

		if ( $amount <= 0 ) {
			LOG::getInstance()->sendWarning("Trying trade small amount: " . $amount);
			return false;
		}

		//Check enough btc for buying
		if ( $btc < $amount * $diff->getPriceBuy() ) {
			$this->notEnoughCoins($this->pair2, $diff, $this->seller->getId() );
			LOG::getInstance()->sendWarning("Not enough ". strtoupper($this->pair2).": ". $btc ." in " . $this->buyer->printName());
			LOG::getInstance()->logDiff( $diff, $this->seller->getCoinNameByPair(), $this->tradeAmount );
			return false;	
		}

		return $amount;
	}

	protected function trade( $diff ) {
		$amount = $this->getAmountForTrade($diff);

		if ( $amount ) {
			if ( $this->trader->trade($amount, $diff->getPriceSell(), $diff->getPriceBuy(), $this->seller, $this->buyer) ) {
				$this->printBalance();
			}

			$this->clearDiffHistory();
			$this->run(1);
			return;
		}

		$this->run(5);
	}

	protected function notEnoughCoins ( $pair, $diff, $stockName ) {
		$this->notRealizedDiff[] = [
			"coin" => $pair,
			"diff" => $diff
		];

		if ( count($this->notRealizedDiff) == 7 ) {
			$this->notifAboutWidthdraw($pair, $stockName);
		}
	}

	protected function printBalance () {
		$i = $this->buyer->getBalance();
		$b = $this->seller->getBalance();
		
		$coinsPair1 = number_format($i->getBalance($this->buyer->getCoinNameByPair()), $this->floatNumbers+1, '.', '');
		$coinsPair2 = number_format($b->getBalance($this->seller->getCoinNameByPair()), $this->floatNumbers+1, '.', '');
		$coinsTotal = number_format($i->getBalance($this->buyer->getCoinNameByPair()) + $b->getBalance($this->seller->getCoinNameByPair()), $this->floatNumbers+1, '.', '');

		$btc1 		= number_format($i->getBalance($this->pair2), 5, '.', '');
		$btc2 		= number_format($b->getBalance($this->pair2), 5, '.', '');
		$btcTotal 	= number_format($i->getBalance($this->pair2)+$b->getBalance($this->pair2), 8, '.', '');
		$bnb 		= null;

		LOG::getInstance()->printBalance([
			array(
				"Coin",
				"Total\t",
				$this->buyer->printName(),
				$this->seller->printName(),
			),
			array(
				"\033[1;37m". strtoupper($this->seller->getCoinNameByPair()) . "\033[0m",
				$coinsTotal.(strlen($coinsTotal) < 6 ? "\t" : ""),
				$coinsPair1.(strlen($coinsPair1) < 6 ? "\t" : ""),
				$coinsPair2.(strlen($coinsPair2) < 6 ? "\t" : "")
			),
			array(
				"\033[1;37mBTC\033[0m",
				number_format($i->getBalance($this->pair2)+$b->getBalance($this->pair2), 5, '.', ''),
				$btc1,
				$btc2
			)
		]);

		$balanceTM = [
			"seller" 	=> $this->buyer->getId(),
			"buyer"		=> $this->seller->getId(),
			"coins"		=> $coinsTotal,
			"coins1"	=> $coinsPair1,
			"coins2"	=> $coinsPair2,
			"btc"		=> $btcTotal,
			"btc1"		=> $btc1,
			"btc2"		=> $btc2,
		];

		LOG::getInstance()->logTradeToTelegram($this->trader->getHistory(), $this->trader->calculateProfit(), $this->seller->getCoinNameByPair(), $balanceTM);

		if ( $btc1 < 0.01 ) { $this->notifAboutWidthdraw($this->pair2, $this->buyer->getId()); }
		if ( $btc2 < 0.01 ) { $this->notifAboutWidthdraw($this->pair2, $this->seller->getId()); }
		if ( $bnb != null && $bnb < 0.25 ) {
			LOG::getInstance()->messageToTelegram('[BALANCE <b>BNB</b>]: ' . $bnb);
		}

		if ( floor($coinsPair2) <= $this->minAmountDoWithdraw && floor($coinsPair2) >= 0 ) {
			$this->doWithdraw( $this->buyer, $this->buyer->getCoinNameByPair(), floor($coinsPair1) );
		}

		if ( floor($coinsPair1) <= $this->minAmountDoWithdraw && floor($coinsPair1) >= 0 ) {
			$this->doWithdraw( $this->seller, $this->seller->getCoinNameByPair(), floor($coinsPair2) );
		}
	}

	public function doWithdraw ( $market, $pair, $amount ) {
		if ( isset($market->withdraw) ) {
			$wa = $market->doWithdraw( $pair, $amount );

			if ( $wa ) {
				print "[". date('H:i:s') ."] \033[32m[WITHDRAW]\033[0m " . "\033[1;37m" . $wa . "\033[0m from " . $market->printName() . "\n";
				LOG::getInstance()->messageToTelegram('✈️  '. strtoupper($market->getCoinNameByPair()) . ' ' . $wa .' from ' . $market->getId());
				return;
			}
		}

		$this->notifAboutWidthdraw( null, $market->getId() );
	}

	protected function beep() {
	    echo "\x07";
	}

	protected function clearDiffHistory () {
		$this->notRealizedDiff = [];
	}

	public function beforeDie () {
		LOG::getInstance()->messageToTelegram('🚨 '. strtoupper($this->buyer->getCoinNameByPair()));
	}

	public function notifAboutWidthdraw( $pair = null, $stockName = '' ) {
		LOG::getInstance()->messageToTelegram('🕹 '. strtoupper($pair != null ? $pair : $this->buyer->getCoinNameByPair()) . " from " . $stockName );
	}

	public function getStockById ( $id ) {
		foreach ($this->stocks as $key => $value) {
			if ( $value->getId() == $id ) { return $value; }
		}

		return null;
	}	
}
?>