<?php 
class Ticker {
	function __construct( $data, $stock = null ) {
        if ( !isset($data) || $data == false ) { return; }

		foreach($data as $k=>$v) {
			$this->$k = $v;
		}

        if ( isset($this->Volume) ) {
            $this->quoteVolume = $this->BaseVolume;
            $this->priceChange = $this->Last - $this->PrevDay;
            $this->priceChangePercent = ($this->Last/$this->PrevDay - 1)*100;
        }

        $this->stock = $stock;
    }

    public function getValueBuy () {
        return $this->bidPrice;
    }

    public function getValueSell () {
        return $this->askPrice;
    }

    public function getAmountBuy () {
        return $this->bidQty;
    }

    public function getAmountSell () {
        return $this->askQty;
    }

    public function getStock () {
        return $this->stock;
    }

    public function getPriceChange () {
        return number_format($this->priceChange, 8, ".", "");
    }

    public function getPriceChangePercent () {
        return number_format($this->priceChangePercent, 2, ".", "");
    }

    public function getVolumeBtc () {
        return number_format($this->quoteVolume, 2, ".", "");
    }
}
?>