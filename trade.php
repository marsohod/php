<?php
	error_reporting(E_ALL);
	ini_set('display_errors', TRUE);
	ini_set('display_startup_errors', TRUE);
	date_default_timezone_set('Asia/Vladivostok');

	require_once("APP.php");
	require_once("settings.php");

	// $SETTINGS = yaml_parse(file_get_contents('settings.yml'));

	if ( !isset( $SETTINGS[strtoupper($argv[1])]) ) {
		print "Wrong COIN name";
		return;
	}

	$APP = new APP($SETTINGS[strtoupper($argv[1])]);

	register_shutdown_function('beforeDie', $APP);
	
	$_ = $_SERVER['_'];

	function beforeDie ( $app ) {
		$app->beforeDie();
		global $_, $argv; 
        pcntl_exec($_, $argv);
	}

	$APP->run();
?>