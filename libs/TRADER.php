<?php
    require_once("./libs/LOG.php");

class TRADER {
    public $pair;
	public $pair2 = 'btc';
	public $stock2;
	protected $history = [];
    protected $buyer;
    protected $seller;

    function __construct( $data ) {
        $this->pair 		= $data['pair'];
		$this->pair2 		= isset($data['pair2']) ? $data['pair2'] : $this->pair2;
		$this->floatNumbers = $data['floatNumbers'];
    }
	/**
	 * @amount - {number}
	 * @priceSell - {float} 
	 * @priceBuy - {float}
	 * @seller - {modelApi} - market
	 * @buyer - {modelApi} - market
	 */
	public function trade ( $amount, $priceSell, $priceBuy, $seller, $buyer ) {
        $this->buyer 	= $seller;
		$this->seller 	= $buyer;
		
		$canTrade		= $this->canTradeByMarket($amount, $priceSell, $priceBuy);

        if ( !$canTrade ) {
			LOG::getInstance()->sendWarning("Cant trade ". $seller->getCoinNameByPair() ." small amount: " . $amount);
			return false;
		}
		
		/** If we can trade by market */
		if ( is_callable($canTrade) ) {
			return $canTrade($seller, $buyer, $amount, $diff );
		}

		return $this->tradeByLimit($amount, $priceSell, $priceBuy, $seller, $buyer);
	}
	
	public function tradeByLimit ( $amount, $priceSell, $priceBuy, $seller, $buyer ) {
		LOG::getInstance()->printStartTrade();

		$soldAmount = $this->sell( $seller, $amount, $priceSell );

		// Sold NO ONE piace of amount
		if ( !$soldAmount ) { return false; }

		// Sold full amount
		if ( $soldAmount == $amount ) { return $this->doBuy($seller, $buyer, $amount, $priceSell, $priceBuy); }

		if ( $this->buyRemainingAmount($amount, $soldAmount, $priceSell, $seller, $buyer) ) { return true; }

		LOG::getInstance()->sendError("Sell order not FULL Filled ". $seller->getCoinNameByPair() ." " . $buyer->printName()."; filled: " . $soldAmount);
		return false;
	}

    /**
	 * If we can trade by market (becouse too small amount) we will get function for trade
	 * @amount - number
	 * @priceSell - number
	 * @priceBuy - number
	 * return false or function for trade
	 */
    protected function canTradeByMarket ( $amount, $priceSell, $priceBuy ) {
		if ( !$this->seller->canTrade($this->seller->getCoinNameByPair(), $amount, $priceSell) &&
				!method_exists($this->seller, 'sellMarket') ) { return false; }

		if ( !$this->seller->canTrade($this->seller->getCoinNameByPair(), $amount, $priceSell) ||
			!$this->buyer->canTrade($this->buyer->getCoinNameByPair(), $amount, $priceBuy) ) {

			if ($this->seller->getId() == "stock1" && method_exists($this->buyer, 'buyMarket') ) {
				return $this->sellByLimitAndBuyByMarket;
			} 

			if ( $this->buyer->canTrade($this->buyer->getCoinNameByPair(), $amount, $priceBuy) ) {
				return $this->sellByMarketAndBuyByLimit;
			}

			return false;
		}

		return true;
	}

	/**
	 * Make correct sell orders
	 * Return sold amount
	 */
	public function sell ( $seller, $amount, $priceSell ) {
		// Make sell order
		$s = $seller->sell( $seller->getCoinNameByPair(), $amount, $priceSell );

		// If order filled immediately
		if ( $s != false && $s->isFilled() ) {
			$this->addHistory( $s, $seller );
			return $amount;
		}
			
		// Wait about 30 sec for filling order
		$s2 = $this->syncOrderFilling( $s->getId(), $seller, 3);

		// If order full filled 
		if ( $s2 ) {
			$this->addHistory( $s2, $seller );
			return $amount;
		}

		// Order haven't filled
		$s = $seller->cancelOrder($s->getId(), $seller->getCoinNameByPair(), 'sell');

		$this->addHistory( $s, $seller );

		if ( $s->isFullUnFilled() ) {
			LOG::getInstance()->sendWarning("Order not FILLED ". $seller->getCoinNameByPair() ." amount ". $amount ." seller ". $seller->printName());
			return false;
		}

		return $this->getAmount( $s->getExecutedQty($this->floatNumbers == 0) );
	}

	protected function sellByLimitAndBuyByMarket( $seller, $buyer, $amount, $priceSell, $priceBuy) {
		LOG::getInstance()->printStartTrade();
		
		$soldAmount = $this->sell( $seller, $amount, $priceSell );

		// Sold NO ONE piace of amount
		if ( !$soldAmount ) { return false; }

		// Sold full amount
		
		if ( $this->buyByMarket( $buyer, $buyer->getCoinNameByPair(), $soldAmount, $priceSell ) ) {
			$this->printEndTrade($soldAmount, $seller, $buyer);
			return true;
		}
		
		if ( $soldAmount == $amount ) { 
			LOG::getInstance()->sendError("SOLD, but not BOUGHT ". $seller->getCoinNameByPair() ." in " . $buyer->printName());
			return false;
		}

		LOG::getInstance()->sendError("Sell order not FULL Filled ". $seller->getCoinNameByPair() ." " . $buyer->printName()."; filled: " . $soldAmount);
		return false;
	}

	protected function sellByMarketAndBuyByLimit( $seller, $buyer, $amount, $priceSell, $priceBuy ) {
		LOG::getInstance()->printStartTrade();

		$s = $seller->sellMarket( $seller->getCoinNameByPair(), $amount );

		if ( $s != false && $s->isFilled() ) {
			// get correct price for order
			$s->setTotalPrice($seller->getMarketOrderPrice($s->getTime(), $seller->getCoinNameByPair()));

			$this->addHistory( $s, $seller );

			if ( $this->doBuy($seller, $buyer, $amount, $diff) ) { return true; }
		}

		return false;
	}

	/**
	 * Return success or fail of buying
	 */
	protected function doBuy( $seller, $buyer, $amount, $priceSell, $priceBuy ) {
		/**
		 * 1. Check amount for ability to trade. If it is too small buy back from SELLER
		 * 2. Trying buy
		 * 3. Cancel order if it is not full filled 
		 * 4. If canceled order is filled (some times it is happend), than trade is success
		 * 5. Check not filled amount for ability to trade.
		 * 5.1 If amount is close equal to 0 than trade is success
		 * 5.2 If amount is small – buy back from SELLER
		 * 6. Trying buy remain amount
		 * 7. Cancel order
		 * 8. Check not filled amount for ability to trade.
		 * 8.1 If amount is close equal to 0 than trade is success
		 * 8.2 If amount is small – buy back from stock2
		 * 9. Trying buy back remain amount from SELLER
		 **/

		// 1
		if ( !$buyer->canTrade($buyer->getCoinNameByPair(), $amount, $priceBuy ) ) {
			if ( $amount == 0 ) { return true; }

			if ($this->buyByMarket( $this->stock2, $this->stock2->getCoinNameByPair(), $amount, $priceSell ) ) {
				$this->printEndTrade($amount, $seller, $this->stock2);
				return true;
			}

			return false;
		}

		// 2
		$b = $this->buy( $buyer, $buyer->getCoinNameByPair(), $amount, $priceBuy );
		
		if ( $b == $amount ) {
			$this->printEndTrade($amount, $seller, $buyer);
			return true;
		}

		// 5
		$a = $this->getAmount( $amount - $b );

		if ( !$buyer->canTrade($buyer->getCoinNameByPair(), $a, $priceBuy ) ) {

			// 5.1
			if ( $amount == 0 || $a == 0 ) {
				$this->printEndTrade($amount, $seller, $buyer);
				return true;
			}

			// 5.2
			if ( $this->buyByMarket( $seller, $seller->getCoinNameByPair(), $a, $priceSell ) ) {
				$this->printEndTrade($b->amountNotFilled( $this->floatNumbers == 0 ), $seller, $seller);
				return true;
			}

			return false;
		}

		// 6
		if ( $this->buyByMarket( $buyer, $buyer->getCoinNameByPair(), $a, $priceSell ) ) {
			$this->printEndTrade($amount, $seller, $buyer);
			return true;
		}

		// 7
		$b = $buyer->cancelOrder($buyer->getLastOrderId(), $buyer->getCoinNameByPair(), 'buy');
		$a = $this->getAmount( $b->amountNotFilled($this->floatNumbers == 0) );
		$this->addHistory( $b, $buyer );

		// 8
		if ( !$seller->canTrade($seller->getCoinNameByPair(), $a, $priceSell ) ) {

			// 8.1
			if ( $amount == 0 || $a == 0 ) {
				$this->printEndTrade($amount, $seller, $buyer);
				return true;
			}

			// 8.2
			if ( $this->buyByMarket( $seller, $seller->getCoinNameByPair(), $a, $priceSell ) ) {
				$this->printEndTrade($a, $seller, $seller);
				return true;
			}

			return false;
		}

		// 9
		if ( $this->buyByMarket( $seller, $seller->getCoinNameByPair(), $a, $priceSell ) ) {
			if ( $a != $amount ) {
				$this->printEndTrade($b->getExecutedQty(), $seller, $buyer);
			}

			LOG::getInstance()->sendError("SELL and BUY for ONE PRICE ". $seller->getCoinNameByPair() ." amount: ". $a ." in " . $seller->printName());
			return true;
		}

		LOG::getInstance()->sendError("SOLD, but not BOUGHT ". $seller->getCoinNameByPair() ." in " . $buyer->printName());
		return false;
	}

	public function buy ( $buyer, $pair, $amount, $price ) {
		// Make buy order
		$s = $buyer->buy( $pair, $amount, $price );

		// If order filled immediately
		if ( $s != false && $s->isFilled() ) {
			$this->addHistory( $s, $buyer );
			return $amount;
		}
			
		// Wait about 30 sec for filling order
		$s2 = $this->syncOrderFilling( $s->getId(), $buyer, 3);

		// If order full filled 
		if ( $s2 ) {
			$this->addHistory( $s2, $buyer );
			return $amount;
		}

		// Order haven't filled
		$s = $buyer->cancelOrder($s->getId(), $pair, 'buy');

		$this->addHistory( $s, $buyer );

		if ( $s->isFullUnFilled() ) {
			LOG::getInstance()->sendWarning("Order not FILLED ". $pair ." amount ". $amount ." buyer ". $buyer->printName());
			return false;
		}

		return $this->getAmount( $s->getExecutedQty($this->floatNumbers == 0) );
	}

	public function buyByMarket ( $buyer, $pair, $amount, $price = null ) {
		$b = $buyer->buyMarket( $pair, $amount, $price );

		$b->setTotalPrice($buyer->getMarketOrderPrice($b->getTime(), $pair));
		
		$this->addHistory( $b, $buyer );

		return $b->isFilled();
	}

	protected function buyRemainingAmount($amount, $soldAmount, $price, $seller, $buyer) {
		// If soldAmount is too small and cant trade by buyer – buy this part my MARKET in stock2.
		if ( !$buyer->canTrade($buyer->getCoinNameByPair(), $soldAmount, $price ) ) {
			if ( $amount == 0 || $soldAmount == 0 ) { return true; }

			if ( $this->buyByMarket( $this->stock2, $this->stock2->getCoinNameByPair(), $soldAmount, $price ) ) {
				$this->printEndTrade($soldAmount, $seller, $this->stock2);
				return true;
			}

			LOG::getInstance()->sendError("Sell ". $seller->getCoinNameByPair() ." " . $seller->printName()." amount: " . $soldAmount . " and CANT BUY " . ($amount-$soldAmount));
			return false;
		}

		// Buy remaining amount by market
		if ( $this->buyByMarket( $buyer, $buyer->getCoinNameByPair(), $soldAmount, $price ) ) {
			$this->printEndTrade($amount, $seller, $buyer);
			return true;
		}

		return false;
	}

	protected function syncOrderFilling ( $orderId, $market, $attempts = 6 ) {
		for ( $i = 0; $i < $attempts; $i++ ) {
			$o = $market->getOrderById( $orderId, $market->getCoinNameByPair() );
			$this->addHistory($o, $market, true);

			if ( $o->isFilled() ) { return $o; }

			sleep(2);
		}

		return false;
	}

	/** HISTORY */
	protected function addHistory ( $order, $market = null, $onlyPrint = false ) {
		if( !$onlyPrint ) {
			$this->history[] = $order;
		}

		LOG::getInstance()->printRow(array(
			$order->getSide()."\t",
			($market ? $market->printName() : "" ),
			$order->isMarket() ? "MARKET" : $order->getPrice(),
			$this->getAmount($order->getOrigQty($this->floatNumbers == 0)),
			$this->getAmount($order->getExecutedQty($this->floatNumbers == 0)),
			$order->getBTC(),
			$order->getStatus()
		));

		return $this->history;
	}

	public function clearHistory () {
		$this->history = [];
	}

	public function getHistory () {
		return $this->history;
	}
	/** ENDHISTORY */

	/** AMOUNT */
	public function cutNum($num, $precision = 2) {
		if ( $num < (1/pow(10,$precision))) { return 0; }

	    return floor($num).substr($num-floor($num),1,$precision+1);
	}

	public function getAmount ( $amount ) {
		if ( $this->floatNumbers == 0 ) {
			return number_format(floor($amount), $this->floatNumbers, '.', '');
		}

		return $this->cutNum($amount, $this->floatNumbers);
	}

	public function calculateProfit () {
		$sellBTC 	= 0;
		$buyBTC 	= 0;

		foreach( $this->history as $k => $o ) {
			if ( $o->isSell() ) { $sellBTC += $o->getBTC(); }
			if ( $o->isBuy() ) 	{ $buyBTC += $o->getBTC(); }
		}

		return number_format($sellBTC-$buyBTC, 8, '.', '');
	}
	/** END AMOUNT */

	protected function printEndTrade ( $amount, $seller, $buyer ) {
		$profit = $this->calculateProfit(); 
		
		LOG::getInstance()->printEndTrade( array(
			"\033[32mSUCCESS\033[0m",
			"-\t",
			"-\t",
			"-",
			$amount,
			"\033[1;37m" . $profit . "\033[0m",
			""
		));

		LOG::getInstance()->logTrade( $this->history, $seller->getCoinNameByPair(), $profit, $seller->getFee(true), $buyer->getFee(true) );
	}
}

?>