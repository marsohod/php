<?php
	require 'vendor/autoload.php';
 
	use Medoo\Medoo;
 	
 	require_once("config.php");

class DB {
	protected $database;
	private static $_instance = null;

	function __construct( $data = null ) {
		$this->database = new Medoo([
		    'database_type' => 'mysql',
		    'database_name' => DB_DATABASE,
		    'server' 		=> DB_SERVER,
		    'username' 		=> DB_USERNAME,
		    'password' 		=> DB_PASSWORD
		]);		
	}

	protected function __clone() {
	// ограничивает клонирование объекта
	}

	static public function getInstance() {
		if( is_null(self::$_instance) ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}
	
	public function putDiff ( $data ) {
		if ( $data['coin'] ) { $data['coin'] = strtoupper($data['coin']); }

		return $this->database->insert('markets_diff', $data);
	}

	public function putOrder ( $data ) {
		return $this->database->insert('orders', $data);
	}

	public function getDiff ( $where = [] ) {
		return $this->database->select('markets_diff', '*', $where);
	}

	public function getLastDiff ( $pair = null ) {
		$out = $this->getDiff([
			"coin" 	=> strtoupper($pair),
			"LIMIT" => 1,
			"ORDER" => ['id' => 'DESC']
		]);

		return isset($out[0]) ? $out[0] : null;
	}

	public function getStocks ( ) {
		return $this->database->select('stocks', [
		    'id',
		    'name'
		]);
	}
}
?>