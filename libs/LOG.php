<?php

require_once("./libs/DB.php");
require_once("./libs/SPINNER.php");
require_once("./model/modelDiff.php");

class LOG {
	private static $_instance = null;
	private $url = [
		"tm" 	=> "",
		"email" => ""
	];
	private $tableBorders = [
		"trade" 	=> "–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––",
		"balance"	=> "–––––––––––––––––––––––––––––––––––––––––––––-–––––––––––",
		"tm_trade"	=> "+––––––––––––––––––––––––––––––––––––––––––––+",
		"trade2"	=> "_________________________"
	];
	
	function __construct( $data = null ) {}

	protected function __clone() {
	// ограничивает клонирование объекта
	}

	static public function getInstance() {
		if( is_null(self::$_instance) ) {
			self::$_instance = new self();
			self::$_instance->SPINNER = new SPINNER();
			self::$_instance->climate = new League\CLImate\CLImate;			
		}

		return self::$_instance;
	}

	/* PRINT CONSOLE */
	public function sendSuccess( $text ) {
		print "[". date('H:i:s') ."] " . "\033[32m[SUCCESS]: \033[0m" . $text . "\n";
	}

	public function sendError( $text ) {
		print "[". date('H:i:s') ."] " . "\033[31m[ERROR]: \033[0m" . $text . "\n";
	}

	public function sendWarning( $text ) {
		print "[". date('H:i:s') ."] " . "\033[1;30m[WARNING]: \033[0m" . $text . "\n";
	}

	public function printRow ( $items, $onlyFormat = false ) {
		$text = "\n| " . implode("	| ", $items) . "	|";

		if ( $onlyFormat ) { return $text; }
		print $text;
	}

	public function printDiff ( $diff, $expectedPercent, $tradeAmount ) {
		$printedDiff = $diff->getPercent() >= $expectedPercent ? "\033[1;37m" . $diff->getPercent() . "%\033[0m" : $diff->getPercent() . "%";

		print "print";
	}

	public function printTableTickers ( $tickers, $diff = null ) {
		$header = [];
		$sell 	= [];
		$buy 	= [];

		for( $i = 0; $i < count($tickers); $i++ ) {
			$header[] = $tickers[$i]->getStock();
			$header[] = $diff != null ? ($i == 0 ? $diff['direction'] : $diff['firstAction']) : '';

			if ( $diff != null && $i == 1 ) {
				$header[] = $diff['priceBuy'];
				$header[] = $diff['priceSell'];
			}

			$sell[] = ($i == 0 ? '👇  ' : '') . $tickers[$i]->getValueSell();
			$sell[] = number_format($tickers[$i]->getAmountSell(), 2, '.', '');

			$buy[] = ($i == 0 ? '👆  ' : '') . $tickers[$i]->getValueBuy();
			$buy[] = number_format($tickers[$i]->getAmountBuy(), 2, '.', '');
		}

		$this->climate->table([
			$header,
			$sell,
			$buy
		]);
	}

	public function printStartTrade () {
		print "\n" . $this->tableBorders["trade"];
		$this->printRow([]);
		print "\n" . $this->tableBorders["trade"];
	}

	public function printEndTrade ( $data ) {
		$this->printRow($data);

		print "\n" . $this->tableBorders["trade"] ."\n\n";
	}

	public function printBalance ( $data ) {
		print "\n" . $this->tableBorders["balance"];

		foreach($data as $k => $tr) { $this->printRow($tr); }

		print "\n" . $this->tableBorders["balance"] . "\n\n";
	}
	/* PRINT CONSOLE END */


	/* INSERT TO DB */
	public function logTrade ( $orders, $pair, $profit = 0, $sComission, $bComission ) {
		return;
		$DB = DB::getInstance();

		foreach( $orders as $k=>$v ) {
			$DB->putOrder(array(
				"side" 			=> $v->isSell() ? 1 : 2,
				"type"			=> $v->isMarket() ? 1 : 0,
				"stock_id" 		=> $v->getStockName(),
				"price"			=> $v->getPrice(),
				"count"			=> $v->getOrigQty(),
				"filled"		=> $v->getExecutedQty(),
				"commission"	=> number_format($v->getBTC() * ($v->isSell() ? $sComission : $bComission), 8, '.', ''),
				"status"		=> $v->getStatus(),
				"coin"			=> $pair
			));
		}
	}

	public function logDiff( $diff, $pair, $tradeAmount ) {
		return;
		if ( $diff->getPercent() < 0.1 ) { return; }

		$pair 		= $pair;
		$amount 	= $diff->getMinAmount($tradeAmount);
		$d 			= DB::getInstance()->getLastDiff( $pair );
		$lastDiff 	= new Diff( $d );

		if ($d != null &&
			$lastDiff->getPercent() == $diff->getPercent() &&
			($lastDiff->getAmountSell() == $diff->getAmountSell() ||
				$lastDiff->getAmountBuy() == $diff->getAmountBuy())) { return; }

		$diff->setCoin( $pair );

		DB::getInstance()->putDiff( (array)$diff );
	}
	/* INSERT TO DB END */


	/* SEND NOTIFICATIONS */
	public function logTradeToTelegram ( $orders, $profit = 0, $pair, $balance = false ) {
		$trade = $this->prepareOrdersToTelegram( $orders, $profit, $pair );

		LOG::getInstance()->messageToTelegram( '#trade' . strtoupper($pair) . "\n" . $trade . "\n". $balance), 'transaction');
	}

	public function prepareOrdersToTelegram ( $orders, $profit = 0, $pair ) {
		$amount 	= 0;
		$sellPrice 	= 0;
		$buyPrice 	= 0;
		$seller 	= '';
		$buyer 		= '';

		foreach( $orders as $k=>$v ) {
			$amount 	+= $v->isSell() ? $v->getExecutedQty() : 0;
			$sellPrice 	= $v->isSell() ? $v->getPrice() : $sellPrice;
			$buyPrice 	= $v->isSell() ? $buyPrice : $v->getPrice();
			
			if ( $v->isSell() ) {
				$seller = strtoupper(substr($v->getStockName(), 0, 3));
			}

			if ( !$v->isSell() ) {
				$buyer = strtoupper(substr($v->getStockName(), 0, 3));
			}
		}

		return "Stocks   : " . $buyer . "-><b>" . $seller . "</b>" .
			"\nProfit     : <b>" . $profit ."</b>" .
			"\nAmount : <b>" . $amount . "</b>".
			"\nSell        : ". $sellPrice  .
			"\nBuy        : " . $buyPrice .
			"\n" . $this->tableBorders["trade2"];
	}

	public function logSignalToTelegram ( $type, $pair, $pair2, $marketId, $url, $amount, $price ) {
		$text = "📉#" . strtoupper($pair) . "\n" .
				"Up signal on <a href='". $url ."'>" . ucfirst($marketId) . "</a>\n" .
				$type . " orders ". $amount ." ". strtoupper($pair2) ."\n" .
				"price: " . $price;

		LOG::getInstance()->messageToTelegram( $text, 'signal');
	}

	public function sendMail ( $params = null ) {
		$params = $params == null ? [] : $params;
		$curl 	= curl_init();

		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($curl, CURLOPT_URL, $this->url["email"] . '?' . implode("&", $params) );

		curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);

		$result = curl_exec($curl);

		curl_close($curl);
	}

	public function messageToTelegram ( $text, $type = false ) {
		switch( $type ) {
			case "signal":
				$group_id = '0';
				break;

			case "transaction":
				$group_id = '0';
				break;

			default:
				$group_id = '0';
				break;
		}

		$curl 	= curl_init();
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 1);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($curl, CURLOPT_URL, str_replace(':group_id', $group_id, $this->url["tm"]) . urlencode($text) );

		curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);

		$result = curl_exec($curl);

		curl_close($curl);
	}

	public function spinner () {
		$this->SPINNER->next();
	}
	/* SEND NOTIFICATIONS END */
}
?>